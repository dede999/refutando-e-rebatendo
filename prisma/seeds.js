const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

async function main() {
  const user = await prisma.admin.upsert({
    where: {
      email: "admin@email.com",
    },
    create: {
      nome: "Admin",
      email: "admin@email.com",
      password: "You won't fond out",
      posts: {
        create: {
          title: "Criando uma publicação teste",
          text: "Uma publicação teste",
          is_external_link: true,
          image_url:
            "https://www.muraldecal.com/en/img/flat018-jpg/folder/products-listado-merchant/wall-stickers-veritas-filia-temporis.jpg",
        },
      },
    },
    update: {},
  });

  const yetAnotherPost = await prisma.post.upsert({
    where: {
      title: "Yet Another Nice Post",
    },
    create: {
      author_email: user.email,
      title: "Yet Another Nice Post",
      text: "I don't have much to say, but I'll say it anyway",
      is_external_link: true,
      image_url:
        "https://www.muraldecal.com/en/img/flat021-jpg/folder/products-listado-merchant/wall-stickers-mens-sana-in-corpore-sano.jpg",
    },
    update: {},
  });

  console.log(user, yetAnotherPost);
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
