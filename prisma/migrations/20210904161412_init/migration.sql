-- CreateTable
CREATE TABLE "Posts" (
    "id" TEXT NOT NULL,
    "author_email" TEXT NOT NULL DEFAULT E'',
    "text" TEXT NOT NULL,
    "image_url" TEXT NOT NULL,
    "is_external_link" TEXT NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Admin" (
    "nome" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "password" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "identifier" ON "Admin"("nome", "email");
