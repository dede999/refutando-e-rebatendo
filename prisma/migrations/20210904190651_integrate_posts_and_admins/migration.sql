/*
  Warnings:

  - You are about to drop the `Posts` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[email]` on the table `Admin` will be added. If there are existing duplicate values, this will fail.

*/
-- DropIndex
DROP INDEX "identifier";

-- DropTable
DROP TABLE "Posts";

-- CreateTable
CREATE TABLE "Post" (
    "id" TEXT NOT NULL,
    "author_email" TEXT NOT NULL DEFAULT E'',
    "text" TEXT NOT NULL,
    "image_url" TEXT NOT NULL,
    "is_external_link" TEXT NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Admin.email_unique" ON "Admin"("email");

-- AddForeignKey
ALTER TABLE "Post" ADD FOREIGN KEY ("author_email") REFERENCES "Admin"("email") ON DELETE CASCADE ON UPDATE CASCADE;
